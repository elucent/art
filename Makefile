.PHONY: art

art:
	g++ -static-libgcc -static-libstdc++ -g3 -L. -L/mingw64/lib -I. -Iinclude GLAD/glad.c art.cpp -o art \
	-lSOIL -lopengl32 -lgdi32 -lkernel32 -luser32 -lcomdlg32 -lglfw3
